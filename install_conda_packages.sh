conda install python=3.5 docopt pandas pytest "rpy2>=2.7.3" bioconductor-deseq2 \
  future "git>=1.8.2" git-lfs sphinx sphinx-autobuild
conda list > conda_packages.txt
